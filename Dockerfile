FROM rocker/verse:4.2
RUN install2.r \
    configr \
    patchwork \
    reshape \
    && rm -rf /tmp/downloaded_packages \
    && Rscript -e 'devtools::install_github("benmarwick/wordcountaddin", type = "source", dependencies = TRUE)' --vanilla